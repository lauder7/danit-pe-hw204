const sPLUS = "+";
const sMINUS = "-";
const sMULTIPLICATION = "*";
const sDIVISION = "/";

lbOperations: {
    let nNumber1 = inputNumber(`Calculation. Enter the first number`);
    if (nNumber1 == null) break lbOperations;

    let nNumber2 = inputNumber(`Enter the second number`);
    if (nNumber2 == null) break lbOperations;

    let sOperationSign = inputOperationSign(`Enter the sign of operation (${sPLUS}, ${sMINUS}, ${sMULTIPLICATION}, ${sDIVISION})`);
    if (sOperationSign == null) break lbOperations;

    console.log(`The result of operation ${nNumber1} ${sOperationSign} ${nNumber2} = ${calcOperation(nNumber1, nNumber2, sOperationSign)}`);
}



function inputNumber(sMsg) {
    let sNum = "";
    let nNum;

    do {
        sNum = prompt(sMsg, sNum);
        if (sNum === null) return null;
        nNum = +sNum;
    } while (sNum === "" || Number.isNaN(nNum));
    
    return nNum;
}



function inputOperationSign(sMsg) {
    let sOp = "";
    let aOps = [sPLUS, sMINUS, sMULTIPLICATION, sDIVISION];

    do {
        sOp = prompt(sMsg, sOp);
        if (sOp === null) return null;
    } while (!aOps.includes(sOp));
    
    return sOp;
}


function calcOperation(nNum1, nNum2, sOp) {
    switch (sOp) {
        case sPLUS: 
            return nNum1 + nNum2; 
        case sMINUS:
            return nNum1 - nNum2; 
        case sMULTIPLICATION:
            return nNum1 * nNum2; 
        case sDIVISION:
            return nNum1 / nNum2; 
        default:
            return undefined;
    }
}
